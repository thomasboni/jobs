# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
### Changed
### Removed
### Fixed

## [0.3.0] - 2021-05-13
### Added
- Add `APIDOC_SOURCE_PATH` variable
- Add `APIDOC_INCLUDE_FILTER` variable
- Add `APIDOC_EXCLUDE_FILTER` variable
- Add `APIDOC_OPTIONS` variable
### Changed
- Upgrade `APIDOC_VERSION` default value

## [0.2.1] - 2020-12-01
### Added
- Enable `artifact:expose_as` option to display job result in merge request

## [0.2.0] - 2020-11-24
### Changed
- Update default output location from `documentation_build/` to `website_build/`

## [0.1.0] - 2020-11-13
### Added
- Initial version
